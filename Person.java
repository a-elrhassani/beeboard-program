import java.util.Date;

public class Person {
    private String name;
    private String address;
    private String postcode;
    private String phone;
    private String creditLimit;
    private String birthday;

    public Person(String name, String address, String postcode, String phone, String creditLimit, String birthday) {
        this.name = name;
        this.address = address;
        this.postcode = postcode;
        this.phone = phone;
        this.creditLimit = creditLimit;
        this.birthday = birthday;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCredit_limit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", postcode='" + postcode + '\'' +
                ", phone='" + phone + '\'' +
                ", creditLimit='" + creditLimit + '\'' +
                ", birthday='" + birthday + '\'' +
                '}';
    }

}
