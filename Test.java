import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * The class Test
 */
public class Test {
    static List<Person> personList = new ArrayList<>();

    /**
     * Create person
     *
     * @param list the list
     */
    public static void createPerson(String[] list) {
        if (list.length == 6) {
            Person p = new Person(list[0], list[1], list[2], list[3], list[4], list[5]);
            personList.add(p);
        }
    }

    /**
     * Gets the csv file info
     *
     * @param ISR the ISR
     * @throws IOException When reading the InputStreamReader fails
     */
    public static void getCsvFileInfo(InputStreamReader ISR) throws IOException {
        try (BufferedReader br = new BufferedReader(ISR)) {
            // To ignore the header of the file
            br.readLine();
            String line;
            // Loop to read lines from the csv File
            while (((line = br.readLine()) != null)) {
                String[] list =  line.replace(",", ";")
                        .replaceFirst("[;]", ",")
                            .split(";");
                // Creating a person object
                createPerson(list);
            }
        }
    }

    /**
     * Format date
     *
     * @param value the value
     * @return String
     * @throws ParseException When parsing dateFormat fails
     */
    public static String formatDate(String value) throws ParseException {
        // Changing the Date format to match the CSV file
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = originalFormat.parse(value);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
        return newFormat.format(date);
    }

    /**
     * Split string to columns
     *
     * @param inputString the input string
     * @return String[]
     * Getting column widths to split them for every line
     */
    public static String[] splitStringToColumns(String inputString, int... ColumnSizes) {
        List<String> list = new ArrayList<>();
        int ColumnStart, ColumnEnd = 0;
        for (int length : ColumnSizes) {
            ColumnStart = ColumnEnd;
            ColumnEnd = ColumnStart + length;
            String dataColumn = inputString.substring(ColumnStart, ColumnEnd);
            list.add(dataColumn.trim());
        }
        return list.toArray(new String[0]);
    }

    /**
     * Format prn file
     *
     * @param ISR the ISR
     *            Formatting the data in the prn file to be like the csv data
     */
    public static void formatPrnFile(InputStreamReader ISR) {
        try (BufferedReader br = new BufferedReader(ISR)) {
            // Ignoring the first line
            br.readLine();
            String line;
            StringBuilder sb;
            // Loop to read the prn file
            while ((line = br.readLine()) != null) {
                sb = new StringBuilder();
                // Calling the splitStringToColumns function to get the content of each column separated
                // The method takes as parameter the ligne and the widths of each column
                String[] parts = splitStringToColumns(line, 16, 22, 9, 14, 13, 8);
                for (int i = 0; i < parts.length; i++) {
                    switch (i) {
                        case 0:
                            // Adding double quotes to the first column
                            parts[i] = "\"" + parts[i] + "\"";
                            break;
                        case 4:
                            // Changing the decimal format of creditLimit
                            parts[i] = String
                                    .valueOf(new DecimalFormat("0.#").format(Double.parseDouble(parts[i]) / 100));
                            break;
                        case 5:
                            // Changing the date fomat of birthday
                            parts[i] = formatDate(parts[i]);
                            break;
                    }
                    sb.append(sb.toString().equals("") ? parts[i] : ";" + parts[i]);
                }
                String[] list = sb.toString().split(";");
                createPerson(list);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    /**
     * Print json
     */
    public static void printJson() {

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Person p : personList) {
            sb.append("{");
            sb.append("\"name\" : ").append(p.getName()).append(",");
            sb.append("\"address\" : ").append(p.getAddress()).append(",");
            sb.append("\"postcode\" : ").append(p.getPostcode()).append(",");
            sb.append("\"phone\" : ").append(p.getPhone()).append(",");
            sb.append("\"credit_limit\" : ").append(p.getCreditLimit()).append(",");
            sb.append("\"birthday\" : ").append(p.getBirthday()).append(",");
            sb.append("},");
        }
        sb.append("}");
        System.out.println(sb);
    }

    /**
     * Print html
     */
    public static void printHtml() {
        StringBuilder sb = new StringBuilder();
        sb.append("<table><thead>").append("\n");
        sb.append("<tr>").append("\n");
        sb.append("<th>Name</th>").append("\n");
        sb.append("<th>Address</th>").append("\n");
        sb.append("<th>Postcode</th>").append("\n");
        sb.append("<th>Phone</th>").append("\n");
        sb.append("<th>Credit card</th>").append("\n");
        sb.append("<th>Birthday</th>").append("\n");
        sb.append("</tr>").append("\n");
        sb.append("<tbody>").append("\n");
        for (Person p : personList) {
            sb.append("<tr>").append("\n");
            sb.append("<td>").append(p.getName()).append("</td>").append("\n");
            sb.append("<td>").append(p.getAddress()).append("</td>").append("\n");
            sb.append("<td>").append(p.getPostcode()).append("</td>").append("\n");
            sb.append("<td>").append(p.getPhone()).append("</td>").append("\n");
            sb.append("<td>").append(p.getCreditLimit()).append("</td>").append("\n");
            sb.append("<td>").append(p.getBirthday()).append("</td>").append("\n");
            sb.append("</tr>").append("\n");
        }
        sb.append("<tbody>");
        sb.append("<table><thead>");
        System.out.println(sb);
    }

    /**
     * Main
     *
     * @param args the args
     * @throws IOException When reading the InputStreamReader fails
     */
    public static void main(String[] args) throws IOException {
        // Testing to first parameter CSV OR PRN
        if (Objects.equals(args[0], "csv")) {
            getCsvFileInfo(new InputStreamReader(System.in, StandardCharsets.ISO_8859_1));
        } else if (Objects.equals(args[0], "prn")) {
            formatPrnFile(new InputStreamReader(System.in, StandardCharsets.ISO_8859_1));
        } else {
            System.out.println("Please enter csv or prn as first parameter");
        }
        // Testing to first parameter JSON OR HTML
        if (Objects.equals(args[1], "json")) {
            printJson();
        } else if (Objects.equals(args[1], "html")) {
            printHtml();
        } else {
            System.out.println("Please enter json or html as second parameter");
        }
    }
}
