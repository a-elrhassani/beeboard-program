# Abdellah El Rhassani
## Data format translation program

I used java to make this program.

## Steps to run the program


First step:

```sh
clone git@gitlab.com:a-elrhassani/beeboard-program.git
```

Second step (Compile the file Test.java):

```java
javac Test.java
```


## Testing the solution

#### For a JSON output:

To test the csv file:
```sh
cat ./Workbook2.csv | java Test csv json > csv.json.txt
```

To test the prn file:

```sh
cat ./Workbook2.prn | java Test prn json > prn.json.txt
```
#### For HTML output:

To test the csv file:

```sh
cat ./Workbook2.csv | java Test csv html > csv.html.txt
```

To test the prn file:

```sh
cat ./Workbook2.prn | java Test prn html > prn.html.txt
```

## Last step

To test the files :

```sh
 diff csv.html.txt prn.html.txt
 ```


## Thank you
